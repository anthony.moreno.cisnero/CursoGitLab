<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <title>CURSO</title>
</head>
<body>
    <nav class="navbar navbar-light bg-dark">
        <a href="#" class="navbar-brand text-light"><h2>CURSO</h2></a>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        </form>
    </nav>
    <div class="container">
        <h1>Git de principiante a experto, GitHub, GitLab, Azure, Commit</h1>
        <p>Commodi ut possimus tenetur dicta, sapiente eligendi laudantium, necessitatibus quisquam suscipit, perspiciatis vero natus doloremque molestiae quod aperiam unde nulla modi illum. Quo inventore accusantium cupiditate eveniet odio consequuntur debitis.
        Eos accusantium veritatis adipisci possimus ipsum ut architecto necessitatibus voluptas non ullam, id amet quia mollitia a facilis aliquam sunt consectetur hic unde molestiae voluptates praesentium expedita eligendi? Et, asperiores!</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et repellendus, consectetur blanditiis dignissimos enim consequatur maiores molestiae voluptatum vel earum molestias at tempore quas eos! Et reprehenderit dignissimos nobis repudiandae!
        Fuga deleniti dolorem vitae odit placeat odio sunt laboriosam maxime, amet exercitationem, sequi illum doloremque aut distinctio, et enim aperiam. Veniam possimus, asperiores nobis cupiditate repellat voluptatem recusandae animi nam!
        Eligendi repudiandae magnam, expedita velit officia animi ea natus consequatur, nulla ab voluptas fugit facilis quo similique sint error voluptates nihil, sequi ipsum numquam alias reiciendis saepe? Neque, deserunt. Deleniti!
        Asperiores deleniti, rerum corporis molestiae reiciendis fuga delectus quia ipsa id atque saepe in ducimus labore itaque earum quas harum ut consequuntur, incidunt non et amet. Molestiae, ea eveniet. Architecto.<br><br>
        Laboriosam perferendis excepturi accusantium quidem, molestias, neque laudantium fugit alias quae quo cum quibusdam officiis odit officia ex hic ducimus! Illo, quos consequatur a molestias quis voluptatibus beatae animi sed.</p>
        <table class="table table-responsive">
            <tr>
                 <th class="text-dark">Titulo 1</th>
                 <th class="text-dark">Titulo 2</th>
                 <th class="text-dark">Titulo 1</th>
                 <th class="text-dark">Titulo 2</th>
                 <th class="text-dark">Titulo 1</th>
                 <th class="text-dark">Titulo 2</th>
                 <th class="text-dark">Titulo 1</th>
                 <th class="text-dark">Titulo 2</th>
            </tr>
            <tr>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
            </tr>
            <tr>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
            </tr>
            <tr>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
            </tr>
            <tr>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
                <td>Contenido 1</td>
                <td>Contenido 2</td>
            </tr>
        </table>       
    </div>
</body>
</html>